import bibtexparser
import copy
import os.path
import re

def latexchem(text):
		'''Remove latex code for chemicals from text, e.g. $CO_2$ -> CO2. Assumes any curly braces {} are already removed'''
		pattern_list = ["\${0,1}[A-Z]{1,2}\${0,1}_[a-z]{0,1}[0-9]{0,1}\${0,1}"]

		remove_list = ['\$', "_"]

		for pattern in pattern_list:
		    match_list = re.findall(pattern, text)
		    for match in match_list:
		        buf = match
		        for remove in remove_list:
		            scrub = re.sub(remove, "", buf)
		            buf = scrub
		            text = text.replace(match, scrub)

		return text

def get_year(bib_copy, year):
		bib_copy.entries = [x for x in bib_copy.entries if int(x['year']) == year]
		return bib_copy

def split_by_year(bibtex_file):

		with open(bibtex_file,'r') as f:
				bib_database = bibtexparser.load(f)

		for year in range(2004, 2021):
				outfile = str(year)+'_'+os.path.basename(bibtex_file)
				with open(outfile,'w') as f:
						bib_copy = copy.deepcopy(bib_database)
						bib_year = get_year(bib_copy, year)
						bibtexparser.dump(bib_year, f)

def get_keyword(bib_copy, keyword):
		bib_entries = []
		for x in bib_copy.entries:
				if 'keyword' in x.keys():
						if keyword in x['keyword']:
								bib_entries.append(x)

		bib_copy.entries = bib_entries

		return bib_copy

def split_by_keyword(bibtex_file, keyword):

		with open(bibtex_file,'r') as f:
				bib_database = bibtexparser.load(f)
				outfile = keyword+'_'+os.path.basename(bibtex_file)

		with open(outfile,'w') as f:
				bib_copy = copy.deepcopy(bib_database)
				bib_keyword = get_keyword(bib_copy, keyword)
				bibtexparser.dump(bib_keyword, f)

		return

def remove_tag(bibtex_file, tagname):

		with open(bibtex_file,'r') as f:
				bib_database = bibtexparser.load(f)
				outfile = os.path.basename(bibtex_file)+'_no'+tagname

		with open(outfile,'w') as f:
				bib_copy = copy.deepcopy(bib_database)

		bib_entries = []
		for x in bib_copy.entries:
				if tagname in x.keys():
						del x[tagname]
				bib_entries.append(x)

		bib_copy.entries = bib_entries
		bib_out = bib_copy
		bibtexparser.dump(bib_out, f)

		return

# examples 

split_by_year('../aura.bib')

# split_by_keyword('../aura.bib', 'omi')

# scrub text for chemical compound latex
# text = "Plankton consume $CO_2$ and make $O_2$. Cars and industry produce $NO_x$."
# check = scrubtext.chemical(text)
# print("in:  "+text)
# print("out: "+check)


